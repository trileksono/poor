package siap.tri.com.poor.view.splash;

import siap.tri.com.poor.model.response.DTOList;
import siap.tri.com.poor.model.response.MJaminan;
import siap.tri.com.poor.view.MvpView;

/**
 * Created by tri on 1/12/17.
 */

public interface SplashView extends MvpView<DTOList<MJaminan>> {
}
