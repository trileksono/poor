package siap.tri.com.poor.view.inputpersonal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import butterknife.Bind;
import com.squareup.picasso.Picasso;
import siap.tri.com.poor.R;
import siap.tri.com.poor.base.BaseActivity;
import siap.tri.com.poor.model.response.DTO;
import siap.tri.com.poor.model.response.MWarga;
import siap.tri.com.poor.util.Constant;
import siap.tri.com.poor.util.ImgCompress;
import siap.tri.com.poor.util.ImgUri;

/**
 * Created by tri on 1/4/17.
 */

public class IPersonalActivity extends BaseActivity implements IPersonalView {
  @Bind(R.id.toolbar) Toolbar mToolbar;
  @Bind(R.id.img_profile) ImageView mImgProfile;
  @Bind(R.id.btn_foto) Button mBtnFoto;
  @Bind(R.id.cont_foto) LinearLayout mContFoto;
  @Bind(R.id.et_nik) TextInputEditText mEtNik;
  @Bind(R.id.btn_cek) Button mBtnCek;
  @Bind(R.id.lnr) LinearLayout mLnr;
  @Bind(R.id.et_nama) TextInputEditText mEtNama;
  @Bind(R.id.lbl_nama) TextInputLayout mLblNama;
  @Bind(R.id.et_no_kk) TextInputEditText mEtNoKk;
  @Bind(R.id.et_alamat) TextInputEditText mEtAlamat;
  @Bind(R.id.txt_rt) TextInputEditText mTxtRt;
  @Bind(R.id.txt_rw) TextInputEditText mTxtRw;
  @Bind(R.id.txt_kelurahan) TextInputEditText mTxtKelurahan;
  @Bind(R.id.txt_kecamatan) TextInputEditText mTxtKecamatan;
  @Bind(R.id.txt_status_kota) TextInputEditText mTxtStatusKota;
  @Bind(R.id.tbl_jaminan) TableLayout mTblJaminan;
  @Bind(R.id.btn_simpan) Button mBtnSimpan;

  private int REQUEST_CAMERA = 100;
  private ImgCompress imgCompress;

  @Override protected int getResourceLayout() {
    return R.layout.activity_i_personal;
  }

  @Override protected void onViewReady(Bundle savedInstance) {

  }

  @Override public Context getContext() {
    return this;
  }

  @Override public void onSuccess(DTO<MWarga> object) {}

  @Override public void onError(String pesan) {}

  @Override public void showProgress(@Nullable String pesan) {}

  @Override public void hideProgress() {}

  @Override public void onOpenCamera(Uri fileUri) {
    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    fileUri = ImgUri.getOutputMediaFileUri();
    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
    startActivityForResult(intent, REQUEST_CAMERA);
  }

  @Override public void onSuccessDTO(DTO dto, boolean isImage) {

  }

  @Override public void onSetImage(String path) {
    Picasso.with(this).load(Constant.IMG_URL+path).error(R.drawable.card_state_pressed)
        .fit().centerCrop().into(mImgProfile);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }
}
