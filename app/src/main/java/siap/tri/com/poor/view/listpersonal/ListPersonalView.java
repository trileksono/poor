package siap.tri.com.poor.view.listpersonal;

import siap.tri.com.poor.model.response.DTOList;
import siap.tri.com.poor.model.response.MWarga;
import siap.tri.com.poor.view.MvpView;

/**
 * Created by TI04 on 12/29/2016.
 */

public interface ListPersonalView extends MvpView<DTOList<MWarga>> {

  void onSuccessPage(DTOList<MWarga> datas, int page);

}
