package siap.tri.com.poor.view.inputpersonal;

import okhttp3.RequestBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import siap.tri.com.poor.App;
import siap.tri.com.poor.base.BasePresenter;
import siap.tri.com.poor.model.response.DTO;
import siap.tri.com.poor.model.response.MWarga;
import siap.tri.com.poor.retrofit.API;

/**
 * Created by tri on 1/4/17.
 */

public class IPersonalPresenter implements BasePresenter<IPersonalView> {

  private IPersonalView mView;
  private DTO<MWarga> mData;
  private DTO mDTO;
  private Subscription mSubscription;

  @Override public void attachView(IPersonalView view) {
    this.mView = view;
  }

  @Override public void detachView() {
    this.mView = null;
    if (mSubscription != null) mSubscription.unsubscribe();
  }

  protected void cariPenduduk(String nik) {
    mView.showProgress("Mencari data..");
    if (mSubscription != null) mSubscription.unsubscribe();
    API service = App.getInstance().getService();
    mSubscription = service.cariPenduduk(nik)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(App.getInstance().defaultSubscribeScheduler())
        .subscribe(new Subscriber<DTO<MWarga>>() {
          @Override public void onCompleted() {
            mView.hideProgress();
            mView.onSuccess(mData);
          }

          @Override public void onError(Throwable e) {

          }

          @Override public void onNext(DTO<MWarga> mWargaDTO) {
            mData = mWargaDTO;
          }
        });
  }

  protected void doUpdate(RequestBody body) {
    mView.showProgress("");
    if (mSubscription != null) mSubscription.unsubscribe();
    API service = App.getInstance().getService();
    mSubscription = service.updatePenduduk(body)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(App.getInstance().defaultSubscribeScheduler())
        .subscribe(new Subscriber<DTO>() {
          @Override public void onCompleted() {
            mView.hideProgress();
            mView.onSuccessDTO(mDTO, false);
          }

          @Override public void onError(Throwable e) {

          }

          @Override public void onNext(DTO dto) {
            mDTO = dto;
          }
        });
  }

  protected void doSimpan(RequestBody body) {
    mView.showProgress("");
    if (mSubscription != null) mSubscription.unsubscribe();
    API service = App.getInstance().getService();
    mSubscription = service.simpanPenduduk(body)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(App.getInstance().defaultSubscribeScheduler())
        .subscribe(new Subscriber<DTO>() {
          @Override public void onCompleted() {
            mView.hideProgress();
            mView.onSuccessDTO(mDTO, false);
          }

          @Override public void onError(Throwable e) {

          }

          @Override public void onNext(DTO dto) {
            mDTO = dto;
          }
        });
  }

  protected void doUpdateFotoPersonal(RequestBody body, String nik) {
    mView.showProgress("");
    if (mSubscription != null) mSubscription.unsubscribe();
    API service = App.getInstance().getService();
    mSubscription = service.updateFotoPersonal(body, nik)
        .observeOn(AndroidSchedulers.mainThread())
        .observeOn(App.getInstance().defaultSubscribeScheduler())
        .subscribe(new Subscriber<DTO>() {
          @Override public void onCompleted() {
            mView.hideProgress();
            mView.onSuccessDTO(mDTO, true);
          }

          @Override public void onError(Throwable e) {
            mView.hideProgress();
          }

          @Override public void onNext(DTO dto) {
            mDTO = dto;
          }
        });
  }

  protected void openCamera(){
    
  }
}
