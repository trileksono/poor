package siap.tri.com.poor.view.splash;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import bolts.Continuation;
import bolts.Task;
import java.util.List;
import org.greenrobot.eventbus.Subscribe;
import siap.tri.com.poor.R;
import siap.tri.com.poor.base.BaseActivity;
import siap.tri.com.poor.model.Event;
import siap.tri.com.poor.model.response.DTOList;
import siap.tri.com.poor.model.response.MJaminan;
import siap.tri.com.poor.util.Constant;
import siap.tri.com.poor.util.DialogUtil;
import siap.tri.com.poor.util.PrefUtil;
import siap.tri.com.poor.view.login.LoginActivity;

/**
 * Created by tri on 1/12/17.
 */

public class SplashActivity extends BaseActivity implements SplashView {

  private SplashPresenter mPresenter;
  private static ProgressDialog mDialog;
  private DTOList<MJaminan> mData;


  @Override protected int getResourceLayout() {
    return R.layout.splash_screen;
  }

  @Override protected void onViewReady(Bundle savedInstance) {
    mPresenter = new SplashPresenter();
    mPresenter.attachView(this);
    mDialog = DialogUtil.waitDialog(this);

    mPresenter.onCallJaminan();

    PrefUtil.setStringPref(Constant.isLogedIn,"1");

    Task.delay(1500).onSuccess(new Continuation<Void, Object>() {
      @Override public Object then(Task<Void> task) throws Exception {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        return null;
      }
    });
  }

  @Override protected void onStart() {
    super.onStart();
    if (!mBus.isRegistered(this)) mBus.register(this);
  }

  @Override protected void onStop() {
    super.onStop();
    mBus.unregister(this);
  }

  @Override protected void onDestroy() {
    mPresenter.detachView();
    super.onDestroy();
  }

  @Override public Context getContext() {
    return this;
  }

  @Override public void onSuccess(DTOList<MJaminan> object) {
    this.mData = object;
    createPrefJaminan(object.getData());
  }

  @Override public void onError(String pesan) {}

  @Override public void showProgress(@Nullable String pesan) {
    mDialog.show();
  }

  @Subscribe public void onEvent(Event event) {
    showToast(event.getPesan());
  }

  @Override public void hideProgress() {
    mDialog.dismiss();
  }

  private void createPrefJaminan(List<MJaminan> mJaminen){
    PrefUtil.setMapPref("prefJaminan", mJaminen);
  }
}
