package siap.tri.com.poor.view.inputpersonal;

import android.net.Uri;
import siap.tri.com.poor.model.response.DTO;
import siap.tri.com.poor.model.response.MWarga;
import siap.tri.com.poor.view.MvpView;

/**
 * Created by tri on 1/4/17.
 */

public interface IPersonalView extends MvpView<DTO<MWarga>> {

  void onOpenCamera(Uri fileUri);

  void onSetImage(String path);

  void onSuccessDTO(DTO dto, boolean isImage);
}
