package siap.tri.com.poor.retrofit;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import siap.tri.com.poor.model.LoginBody;
import siap.tri.com.poor.model.response.DTO;
import siap.tri.com.poor.model.response.DTOList;
import siap.tri.com.poor.model.response.MJaminan;
import siap.tri.com.poor.model.response.MKeluarga;
import siap.tri.com.poor.model.response.MLogin;
import siap.tri.com.poor.model.response.MWarga;

/**
 * Created by TI04 on 11/10/2016.
 */

public interface API {

  @POST("api/login") Observable<DTO<MLogin>> RLogin(@Body LoginBody body,
      @Header("Content-Type") String contentType);

  //
  //@GET("api/findKeluarga?")
  //Call<DTO<CariKeluarga>> findKeluarga(@Query("nik") String nik);
  //
  @GET("api/jaminan?") Observable<DTOList<MJaminan>> getJaminan(@Query("tipe") String tipe);

  @GET("api/cariPenduduk") Observable<DTO<MWarga>> cariPenduduk(@Query("nik") String nik);

  //@GET("api/findPenduduk")
  //Call<DTO<MWarga>> findPenduduk(@Query("nik") String nik);
  //
  @POST("api/jPersonal") Observable<DTO> simpanPenduduk(@Body RequestBody requestBody);

  @POST("api/updateJPersonal") Observable<DTO> updatePenduduk(@Body RequestBody requestBody);

  @POST("api/profilePenduduk/{nik}") Observable<DTO> updateFotoPersonal(
      @Body RequestBody requestBody, @Path("nik") String nik);

  //@GET("api/findJKeluarga")
  //Call<DTO<CariKeluarga>> cariJKeluarga(@Query("kk") String kk);
  //
  //@POST("api/jKeluarga")
  //Call<DTO> saveJKeluarga(@Body RequestBody requestBody);
  //
  //@POST("api/updateJKeluarga")
  //Call<DTOList<KriteriaRes.DatasBean>> updateJKeluarga(@Body RequestBody requestBody);
  //
  //@POST("api/addFotoKeluarga/{kk}")
  //Call<DTOList<FotoKeluarga>> tambahFoto(@Body RequestBody requestBody, @Path("kk") String kk);
  //
  //@POST("api/kriteriaKeluarga")
  //Call<DTO> saveKriteria(@Body RequestBody body);
  //
  //@GET("api/getKriteriaKeluarga")
  //Call<DTO<KriteriaRes>> getAllKriteria(@Query("kk") String kk);
  //
  //@POST("api/updateKriteriaKeluarga")
  //Call<DTO> updateKriteriaKeluarga(@Body List<KriteriaReq> list);
  //
  //@POST("api/updateFotoLantai")
  //Call<DTOList> updateFotoLantai(@Body RequestBody body);
  //
  //@POST("api/updateFotoAtap")
  //Call<DTOList> updateFotoAtap(@Body RequestBody body);
  //
  //@POST("api/updateFotoDinding")
  //Call<DTOList> updateFotoDinding(@Body RequestBody body);
  //
  //@POST("api/updateFotoMck")
  //Call<DTOList> updateFotoMck(@Body RequestBody body);
  //
  @GET("api/getKeluarga/{page}?") Observable<DTOList<MKeluarga>> getKeluargaPagging(
      @Path("page") int page, @Query("noKk") String kk);

  @GET("api/getPersonal/{page}?") Observable<DTOList<MWarga>> getPersonalPagging(
      @Path("page") int page, @Query("nik") String nik);
}
