package siap.tri.com.poor.view.splash;

import org.greenrobot.eventbus.EventBus;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import siap.tri.com.poor.App;
import siap.tri.com.poor.R;
import siap.tri.com.poor.base.BasePresenter;
import siap.tri.com.poor.model.Event;
import siap.tri.com.poor.model.response.DTOList;
import siap.tri.com.poor.model.response.MJaminan;
import siap.tri.com.poor.retrofit.API;

/**
 * Created by tri on 1/12/17.
 */

public class SplashPresenter implements BasePresenter<SplashView> {

  private SplashView mView;
  protected DTOList<MJaminan> mData;
  private Subscription mSubscription;

  @Override public void attachView(SplashView view) {
    this.mView = view;
  }

  @Override public void detachView() {
    this.mView = null;
    if(mSubscription != null) mSubscription.unsubscribe();
  }

  protected void onCallJaminan(){
    mView.showProgress(null);
    if(mSubscription != null) mSubscription.unsubscribe();
    API service = App.getInstance().getService();
    mSubscription = service.getJaminan("").subscribe(new Subscriber<DTOList<MJaminan>>() {
      @Override public void onCompleted() {
        mView.hideProgress();
        mView.onSuccess(mData);
      }

      @Override public void onError(Throwable e) {
        mView.hideProgress();
        if (isHttp404(e)) {
          EventBus.getDefault().post(new Event(mView.getContext().getString(R.string.error404)));
        } else {
          EventBus.getDefault().post(new Event(mView.getContext().getString(R.string.http_error)));
        }
      }

      @Override public void onNext(DTOList<MJaminan> mJaminanDTOList) {
        mData = mJaminanDTOList;
      }
    });
  }

  private static boolean isHttp404(Throwable error) {
    return error instanceof HttpException && ((HttpException) error).code() == 404;
  }
}
